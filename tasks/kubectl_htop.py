import subprocess
import time
from curses import wrapper
from datetime import datetime
import sys
from invoke import task

ARGS = []

def main(stdscr):
    number_arg = 0
    stdscr.clear()
    for arg in ARGS:
        number_arg = print_top(arg, number_arg, stdscr)
        number_arg += 2
    now = datetime.now()
    stdscr.addstr(number_arg, 0, now.strftime("%d/%m/%Y %H:%M:%S"))
    stdscr.refresh()
    time.sleep(1)

def print_top(resources: list, number_arg: int, stdscr):
    resource_type = resources[0]
    for resource in resources[1:]:
        cmd = f"kubectl top {resource_type} {resource}"
        cmd_stdout = subprocess.run(cmd, shell=True, check=True, capture_output=True)
        stdscr.addstr(number_arg, 0, cmd_stdout.stdout.decode('utf8'))
        number_arg += 2
    return number_arg


@task
def top_kube(c, pods="", nodes=""):
    """To show diffante pods or nodes use "," beetwen."""
    pod_arg = ['pod']
    node_arg = ['node']
    if pods:
        pod_arg += pods.split(',')
        ARGS.append(pod_arg)
    if nodes:
        node_arg += nodes.split(',')
        ARGS.append(node_arg)
    in_loop = True
    while in_loop:
        wrapper(main)
