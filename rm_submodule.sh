#!/bin/bash

SUBMODULE_PATH=$1

git submodule deinit -f -- $SUBMODULE_PATH
rm -rf .git/modules/$SUBMODULE_PATH
git rm -f $SUBMODULE_PATH
rm -rf $SUBMODULE_PATH
