# Work tools

## Kubectl Top

To have a view of pod and node consummation

Usage :
`invoke kubectl-htop.top-kube -p PODS_NAME -n NODES_NAME`

## RM submodule

To remove easily a submodule

Usage :

* Go to the project directory
* `sh rm_submodule.sh SUBMODULE_NAME_TO_REMOVE`


### Alias
alias top_kubectl="cd PATHS && invoke PATHS/kubectl-htop.top-kube"
alias rm_submodule="sh ~/Work/tools/rm_submodule.sh"

alias panoply="sh ~/.PanoplyJ/panoply.sh"
